import React from 'react';
import Footer from "./Footer";

const Nature = () => {
    return <div id="nature">
        <h1>Nature Paintings</h1>
        <div>
            <img src="Beach2.jpg" alt="nature" width="100px"></img>
            <img src="Durdle Door.jpg" alt="nature" width="100px"></img>
            <img src="Exotic Sunset2.jpg" alt="nature" width="100px"></img>
            <img src="Forest2.jpg" alt="nature" width="100px"></img>
            <img src="From Above2.jpg" alt="nature" width="100px"></img>
            <img src="High in the clouds2.jpg" alt="nature" width="100px"></img>
            <img src="Moonlight2.jpg" alt="nature" width="100px"></img>
            <img src="paint5.jpg" alt="nature" width="100px"></img>
            <img src="The Fall2.jpg" alt="nature" width="100px"></img>
            <img src="Waterfall2.jpg" alt="nature" width="100px"></img>
            <img src="Winter World2.jpg" alt="nature" width="100px"></img>
        </div>
        <Footer />
    </div>

}

export default Nature;

