import React from 'react';
import Footer from "./Footer";

const Abstract = () => {
    return <div id="abstract">
        <h1>Abstract Paintings</h1>
        <img src="Fireplace2.jpg" alt="abstract" width="100px"></img>
        <img src="Melting.jpg" alt="abstract" width="100px"></img>
        <img src="Paint1.jpg" alt="abstract" width="100px"></img>
        <img src="Paint3.jpg" alt="abstract" width="100px"></img>
        <img src="Paint4.jpg" alt="abstract" width="100px"></img>
        <img src="Paint6.jpg" alt="abstract" width="100px"></img>
        <img src="Paint7.jpg" alt="abstract" width="100px"></img>
        <img src="Park way2.jpg" alt="abstract" width="100px"></img>
        <img src="The all seeing eye2.jpg" alt="abstract" width="100px"></img>
        <img src="The ghost.jpg" alt="abstract" width="100px"></img>
        <img src="The Mind Beyond2.jpg" alt="abstract" width="100px"></img>
        <img src="The Night.jpg" alt="abstract" width="100px"></img>
        <img src="The Roots.jpg" alt="abstract" width="100px"></img>
        <img src="The Shine.jpg" alt="abstract" width="100px"></img>
        <img src="Worlds End.jpg" alt="abstract" width="100px"></img>
        <Footer />
    </div>

}

export default Abstract;

