import React from 'react';
import Footer from './Footer';
import ShopButton from './ShopButton';

const Contact = () => {
    return (
        <div id="contact">
            <h1>Get in touch</h1>
            <Footer />
        </div>
        
    );
}
export default Contact;

  
   