import React from 'react';
import Footer from "./Footer";
import ShopContainer from './ShopContainer';

const Shop = () => {
    return (
        <div id="contact">
            <h1>Your Shop</h1>
         <ShopContainer/>
            <Footer />
        </div>

    );
}
export default Shop;


