import React from 'react';
import { Router } from "@reach/router";
import Home from "./components/Home";
import MenuContainer from "./components/MenuContainer";
import About from "./components/About";
import Portfolio from "./components/Portfolio";
import Contact from "./components/Contact";
import ShopContainer from './components/ShopContainer';
import Categories from "./components/Categories";
import Nature from "./components/Nature";
import Abstract from "./components/Abstract";
import Portraits from "./components/Potraits";

function App() {
  return (
    <div className="App">
      <ShopContainer />
      <MenuContainer />
      <Categories/>
      <Router>
        <Home path="/" exact component={Home} />
        <About path="/about" exact component={About} />
        <Portfolio path="/portfolio" exact component={Portfolio} />
        <Contact path="/contact" exact component={Contact} />
        <Nature path="/nature" exact component={Nature} />
        <Abstract path="/abstract" exact component={Abstract} />
        <Portraits path="/portraits" exact component={Portraits}/>
      </Router>
    </div>
  );
}

export default App;
